# Fizz Buzz Kata

## Goal

Escribir un programa que escribe por pantalla los números del 1 al 100, pero para múltiplos de tres tiene que escribir "Fizz" en vez del número y para múltiplos de cinco tiene que escribir "Buzz" Para números que son multiples de tres y de 5 a la vez tiene que escribir "FizzBuzz".

*Sample output*

	1
	2
	Fizz
	4
	Buzz
	Fizz
	7
	8
	Fizz
	Buzz
	11
	Fizz
	13
	14
	FizzBuzz
	16
	17
	Fizz
	19
	Buzz
	... etc up to 100

## Object calisthenics, 9 restricciones:

1. solo un nivel de identación por método

2. no se permite el uso de ELSE

3. encapsular todos los tipos primitivos en objetos

4. cualquier clase que contenga colecciones no debe contener otros atributos

5. solo un punto por línea

6. no se permiten abreviaciones

7. mantener todas las entidades pequeñas

8. no se permite más de dos variables de instancia (atributos)

9. no se permiten getters, setters ni properties

https://williamdurand.fr/2013/06/03/object-calisthenics/


## Install

```
yarn install
```

## Run tests

```
yarn test
```

## Avoid intellij warnings

Go to *Settings/Languages & frameworks/JavaScript/Libraries* and download:
* @types/node
* @types/jest
