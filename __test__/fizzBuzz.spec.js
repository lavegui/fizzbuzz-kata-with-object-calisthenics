import fizzBuzz from "../src/fizzBuzz";

describe('fizz buzz kata', () => {

    it('it is the expected name', () => {
        expect(fizzBuzz().whoAmI).toBe('fizz buzz');
    });
});
